//
// Created by Stephen Clyde on 4/12/17.
//

#include "Gadget.h"

void Gadget::printDetails(std::ostream& out)
{
    out << "Weight" <<std::endl;
    out << "------" <<std::endl;
    out << m_weight << std::endl;
}
//
// Created by Stephen Clyde on 4/12/17.
//

#ifndef TEMPLATEMETHODEXAMPLE_GADGET_H
#define TEMPLATEMETHODEXAMPLE_GADGET_H

#include "Component.h"

class Gadget : public Component {
private:
    double  m_weight  = 0;

public:
    Gadget(int id, std::string title, double weight) : Component(id, title), m_weight(weight) {}

    void printDetails(std::ostream& out);

};


#endif //TEMPLATEMETHODEXAMPLE_GADGET_H

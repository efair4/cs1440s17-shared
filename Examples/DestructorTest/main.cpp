#include <iostream>

class Widget
{
public:
    ~Widget() { std::cout << "Widget Destructor Called" << std::endl; }
};

class Container
{
public:
    ~Container() { std::cout << "Container Destructor Called" << std::endl; }

private:
    Widget* myWidget[10];
};

void f()
{
    Container myContainer;
}

int main() {
    f();
    return 0;
}
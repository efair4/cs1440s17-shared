//
// Created by Stephen Clyde on 3/20/17.
//

#ifndef SIMPLE_TEMPLATES_MAX_TEMPLATE_H
#define SIMPLE_TEMPLATES_MAX_TEMPLATE_H


template<typename X> X max(X a, X b)
{
    X result = a;
    if (b>a)
        result = b;
    return result;
}


#endif //SIMPLE_TEMPLATES_MAX_TEMPLATE_H

//
// Created by Stephen Clyde on 3/29/17.
//

#ifndef ABSTRACTCLASSES_WIDGET_H
#define ABSTRACTCLASSES_WIDGET_H

#include "PrintableThing.h"

class Widget : public PrintableThing {
private:
    int m_width = 0;
    int m_height = 0;

public:
    Widget(int width, int height);
    void print(std::ostream& out);

    int getWidth() const { return m_width; }
    int getHeight() const { return m_height; }
};


#endif //ABSTRACTCLASSES_WIDGET_H
